README
======
This repository provides a featuer-rich dataset of eight users for recommending long-tail content. Besides the collected log data, You can find the tool, with which the data was collected, a screencast 
of a part of the collection procedure from one participant (to get an intuition of it) and additional data, which was collected with accompanying questionnaires.

dataCollectionTool.crx
======================
The chrome extension with which the data was collected

instructions.pdf
================
Transcript of the instructions for participants

Tasks_en.pdf
============
Transcript of exemplary task assignments

Task_A2_en.mp4
==============
A screencast of the predefined task T.A2.en, executed by a user.

responses.csv and variable-coding.txt
=====================================
The responses of a questionnaire accompanying the data collection are contained in responses.csv. These contain demographic information about a user, along with questions on privacy and an assessment of the interface.
The file variable-coding lists the questions and demographic fields corresponding to the variables in responses.csv.

dataset.zip
===========
The dataset consists of several zip-files, one for each user. Each of these zip-files contains the following files, which represent
parts of the user's interactions in json format.

* **tasks.json** the tasks executed
* **history.json** the browsing history
* **interactions.json** log of interactions, such as mouseclicks and textual input
* **queries.json** the queries issued
* **resource_relations.json** log of interactions with retrieved results, in particular views, ratings, annotations
* **recommendations.json** the retrieved search results
* **browserInfo.json** information about the browser/operating system

Besides the file browserInfo.json, all data is exported from object stores in an indexed database and therefore in the 
format of key/value-pairs. The keys are mostly auto-incremented integer values. In the following, the file content are described in more detail.
In particular, for the data exported from object stores, an exemplary entry is provided with comments on the contained attributes.


tasks.json
----------
This file contains the tasks that were executed by a user, along with start and end times and relevant topics. DBpedia categories are added to the topics when available.
exemplary task:
```javascript
{
		"task_id" : 26, // auto-incremented key identifier
		"end" : 1390932307000, // end time of the task in ms since the epoch
		"name" : "annotate webpage", // type of the task ("annotate webpage", "write a blog entry" or "other")
		"topics" : [{ // the topics relevant to this task
				"uri" : "http://dbpedia.org/resource/Category:Cold_War", // DBpedia category (if available)
				"label" : "Cold War" // user provided topic label
			}, {
				"label" : "german democratic repupublic"
			}, {
				"label" : "western germany"
			}, {
				"uri" : "http://dbpedia.org/resource/Category:Soviet_Union",
				"label" : "Soviet Union"
			}, {
				"label" : "frontier soldiers"
			}, {
				"label" : "victims"
			}, {
				"label" : "SED"
			}, {
				"uri" : "http://dbpedia.org/resource/Category:1989",
				"label" : "1989"
			}
		],
		"start" : 1390929987000, // start time of the task
		"individual" : "T.A8.de", // specific name of the task (identifier for predefined tasks or freely chosen label for task "other")
		"expertise_level" : 4, // level of the user's self-assessed level of expertise on the topic (scale 0-10, 0:= lowest)
		"recommendations_desirable" : true // boolean indicator if recommendations are desirable for this task (always true for predefined tasks, specified by the user at tasks of her own choice)
	}
```

history.json
------------
The browsing history, in which each visit has the following format (exemplary visit):
```javascript
{
		"visit_id" : 344, // the auto-incremented key identifier
		"end" : 1388609927076, // end time of the visit in ms since the epoch 
		"task_id" : 21, // identifier of the associated task
		"url" : "http://en.wikipedia.org/wiki/Pope", // url of the visited page
		"referrer" : "", // the referrer, which lead to this visit (if any)
		"transition" : "typed", // the transition, how the browser navigated to this url (link, typed, reload, ... see https://developer.chrome.com/extensions/history)
		"start" : 1388609925310, // start time of the visit in ms since the epoch
		"task_name" : "T.A6.en", // name of the associated task
		"chrome_visitId" : "398" // identifier of the corresponding visit in chrome's history
}
```
A visit lasts, as long as the visited page has the focus. This means, when the user switches the tab in the browser, the
visit of the first tab ends and a new visit starts for the web page in focus of the current tab. The same applies, if
the browser window looses the focus and therefore, the visit ends as well.

interactions.json
-----------------
This file contains a log of the user's interactions on a web page, in particular mouse clicks and textual input. Text which was entered without explicitly submitting a form got logged after a timeout of one second.

### type:click ###
```javascript
{
		"id" : 1157,  // the auto-incremented key identifier
		"task_id" : 13, // identifier of the associated task
		"target" : "id(\"edbtn__save\")", // the target of the click, represented as XPATH
		"url" : "http://mics.fim.uni-passau.de/wiki/doku.php?id=eexcess-test:006&do=draft", // url of the page on which the click occured
		"timestamp" : 1388443253511, // timestamp in ms since the epoch
		"eexcess_visible" : true, // boolean indicator if the eexcess-sidebar was visible at the time of the click
		"bodyHeight" : 988, // the height of the page-body in px
		"bodyWidth" : 1570, // the width of the page-body in px
		"task_name" : "T.B1", // the name of the associated task
		"pageY" : 495, // the y position of the click in the document (in px relative to the top-left corner)
		"pageX" : 238, // the x position of the click in the document (in px relative to the top-left corner)
		"type" : "click" // the type of interaction (always 'click' in this case obviously)
	}
```
### type:textInput ###
```javascript
{
		"id" : 157, // the auto-incremented key identifier
		"task_id" : 4, // identifier of the associated task
		"url" : "http://www.youtube.com/", // the url of the page on which the textual input occured
		"text" : "rieding op 21", // the text that was written
		"eexcess_visible" : true, // boolean indicator if the eexcess-sidebar was visible at the time of the click
		"task_name" : "youtube", // name of the associated task
		"timestamp" : 1388351848751, // timestamp in ms since the epoch
		"type" : "textInput" // the type of interaction (always 'textInput' in this case obviously)
}
```
### type:submit ###
```javascript
{
		"id" : 2010, // the auto-incremented key identifier
		"task_id" : 24, // identifier of the associated task
		"target" : "id(\"dw__login\")", // XPATH of the target of the submit-event
		"parameters" : [{ // serialization of the form-parameters (serialization is basically the same as with jQuery.serializeArray(), but ignoring password fields)
				"name" : "sectok",
				"value" : "6112fe3398fca3f21a24bbfd946c1543"
			}, {
				"name" : "id",
				"value" : "eexcess-test:006"
			}, {
				"name" : "do",
				"value" : "login"
			}, {
				"name" : "u",
				"value" : "eexcesstest006"
			}
		],
		"url" : "http://mics.fim.uni-passau.de/wiki/doku.php?id=eexcess-test:006&do=login&sectok=6112fe3398fca3f21a24bbfd946c1543", // the url of the corresponding page
		"timestamp" : 1388703168687, // timestamp in ms since the epoch
		"eexcess_visible" : true, // boolean indicator if the eexcess-sidebar was visible at the time of the submit-event
		"task_name" : "T.B3", // name of the associated task
		"type" : "submit" // the type of interaction (always 'submit' in this case obviously)
}
```

queries.json
------------
the queries issued alongside with the corresponding text selection and the paragraphs in the viewport at that point of time.
exemplary query:
```javascript
{
		"id" : 266, // auto-incremented key identifier
		"task_name" : "T.A7.en", // name of the corresponding task
		"task_id" : 25, // identifier of the corresponding task
		"timestamp" : 1386778076662, // timestamp of when the query was issued in ms since the epoch
		"context" : { // the context of the query
			"url" : "http://www.britannica.com/EBchecked/topic/219315/French-Revolution", // the url of the page viewed when the query was issued
			"selectedText" : "On July 14, 1789, the Parisian crowd seized the Bastille, a symbol of royal tyranny. ", // the selected text
			"paragraphs" : [ // the visible paragraphs
			"The Estates-General met at", "on May 5, 1789 ..."] // actual paragraphs omitted in this example
		},
		"query" : "storming Bastille 1789" // the query
}
```

resource_relations.json
-----------------------
This file contains the interactions with the retrieved results, in particular
- viewing a result
- rating a result
- annotating a selected text passage with the result

### type:view ###
exemplary view:
```javascript
{
		"id" : 340, // auto-incremented key identifier
		"resource" : "http://www.europeana.eu/portal/record/9200137/oai_kb_dk_oai_kb_dk_images_billed_2010_okt_billeder_object167758.html?utm_source=api&utm_medium=api&utm_campaign=HT6JwVWha", // uri of the viewed resource
		"task_name" : "T.A1.en", // name of the associated task
		"timestamp" : 1389721156824, // start time of the view in ms since the epoch
		"beenRecommended" : true, // boolean indicator if the resource was recommended by EEXCESS (always true in this dataset)
		"context" : { // the context, in which the resource was viewed
			"query" : "World War I", // the query, which retrieved the resource
			"task_id" : 4 // identifier of the associated task
		},
		"duration" : 5116, // duration of the view in ms
		"type" : "view" // type of the relation
}
```
### type:rating ###
exemplary rating:
```javascript
{
		"id" : 345, // auto-incremented key identifier
		"beenRecommended" : true, // boolean indicator, if the resource was recommended by EEXCESS (always true in this dataset)
		"type" : "rating", // type of the relation
		"resource" : "http://www.europeana.eu/portal/record/04802/084040647A60C52544BA7BF0D249E3FC93837043.html?utm_source=api&utm_medium=api&utm_campaign=HT6JwVWha", // uri of the rated resource
		"task_name" : "T.A1.en", // name of the associated task
		"timestamp" : 1389721586267, // timestamp in ms since the epoch
		"annotation" : { // the rating in Open Annotation format (see http://www.openannotation.org/spec/core/)
			"@context" : "http://www.w3.org/ns/oa-context-20130208.json",
			"hasTarget" : {
				"@id" : "http://www.europeana.eu/portal/record/04802/084040647A60C52544BA7BF0D249E3FC93837043.html?utm_source=api&utm_medium=api&utm_campaign=HT6JwVWha",
				"@type" : "dctypes:undefined"
			},
			"hasBody" : {
				"http://purl.org/stuff/rev#minRating" : 1,
				"http://purl.org/stuff/rev#maxRating" : 2,
				"http://purl.org/stuff/rev#rating" : 1
			},
			"@type" : "oa:Annotation"
		},
		"context" : { // the context in which the rating was given
			"query" : { // the query, which retrieved the resource
				"lang" : "all", // language filter for query results
				"query" : "Weimar Republic" // the query
			},
			"task_id" : 4 // identifier of the associated task
		}
}
```
### type:annotation ###
exemplary annotation:
```javascript
{
		"id" : 344, // auto-incremented key identifier
		"resource" : "en.wikipedia.org/wiki/Munich", // the annotated resource (url of the page containing the selected text)
		"task_name" : "T.A1.en", // name of the associated task
		"type" : "annotation", // type of the relation
		"task_id" : 4, // identifier of the corresponding task
		"timestamp" : 1389721204029, // timestamp in ms since the epoch
		"annotation" : { // the annotation in Open Annotation format (see http://www.openannotation.org/spec/core/)
			"@context" : "http://www.w3.org/ns/oa-context-20130208.json",
			"oa:motivatedBy" : "oa:tagging",
			"hasBody" : [{
					"@id" : "http://europeana.eu/api/v2/record/9200137/oai_kb_dk_oai_kb_dk_images_billed_2010_okt_billeder_object154263.json?wskey=HT6JwVWha",
					"@type" : "oa:SemanticTag"
				}
			],
			"@type" : "oa:Annotation",
			"hasTarget" : {
				"oa:hasSource" : {
					"@id" : "en.wikipedia.org/wiki/Munich",
					"@type" : "dctypes:text"
				},
				"hasSelector" : {
					"oa:prefix" : ").",
					"oa:suffix" : " to World War II",
					"@type" : "oa:TextQuoteSelector",
					"oa:exact" : "World War I"
				},
				"@type" : "oa:SpecificResource"
			}
		},
		"ranges" : [{ // additional identifier of the selected text, based on XPATH
				"start" : "id(\"World_War_I_to_World_War_II\")",
				"end" : "id(\"World_War_I_to_World_War_II\")",
				"startOffset" : 0,
				"endOffset" : 11
			}
		]
}
```

recommendations.json
--------------------
This file contains a log of all the results retrieved by the queries issued by the user.
exemplary recommendation:
```javascript
{
		"recommendation_id" : 4446, // auto-incremented key identifier
		"task_name" : "T.A2.en", // name of the associated task
		"task_id" : 9, // identifier of the associated task
		"timestamp" : 1389807747100, // timestamp of when the result was retrieved in ms since the epoch
		"uri" : "http://www.europeana.eu/portal/record/2023848/_http___keptar_oszk_hu_035900_035931__.html?utm_source=api&utm_medium=api&utm_campaign=HT6JwVWha", // uri of the result
		"context" : { // the context in which the result was retrieved
			"query" : { // the query, for which the result was retrieved
				"lang" : "all", // language filter for search results
				"query" : " Otto von Bismarck" // the query
			},
			"task_id" : 9 // identifier of the associated task
		}
}
```

browserInfo.json
----------------
The browser/system information, such as operating system, plugins/fonts installed, etc. which is contained in the 
window.navigatior object of the browser. This information is serialized to JSON with https://github.com/Canop/JSON.prune
to avoid serialization overflow. Thus, the object is not present to full depth.
In addition a serialiation of the jQuery.support properties collection is contained in this file.

